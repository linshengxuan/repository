#include <iostream>
using namespace std;

int n, x;

int lowbit (int x) {
    return x & ~x + 1;
}

void fun (int x) {
    int cnt = 0;
    while ( x ) x -= lowbit (x), cnt ++ ;
    cout << cnt << ' ';
}

int main () {

    cin >> n;
    while ( n -- ) {
        cin >> x;
        fun (x);
    }

    return 0;
}