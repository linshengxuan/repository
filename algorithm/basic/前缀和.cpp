#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int n, m;
int q[N];

void fun () {
    for ( int i = 1; i <= n; i ++ ) q[i] += q[i - 1];
}

int main () {

    cin >> n >> m;
    for ( int i = 1; i <= n; i ++ ) cin >> q[i];

    fun ();
    
    int l, r;
    while ( m -- ) {
        cin >> l >> r;
        cout << q[r] - q[l - 1] << endl;
    }

    return 0;
}