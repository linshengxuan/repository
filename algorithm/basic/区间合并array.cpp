#include <iostream>
#include <algorithm>
using namespace std;

const int N = 1e5 + 10;

typedef pair<int, int> pii;
#define l first
#define r second

int n, ans;
pii a[N];

int main () {

    cin >> n;
    for ( int i = 0; i < n; i ++ ) cin >> a[i].l >> a[i].r;
    sort (a, a + n);

    int start = -2e9, end = -2e9;
    for ( int i = 0; i < n; i ++ ) {
        if ( end < a[i].l ) {
            ans ++ ;
            start = a[i].l;
            end = a[i].r;
        } else end = max (a[i].r, end);
    }
    cout << ans << endl;

    return 0;
}