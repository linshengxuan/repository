#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int n, ans;
vector < pair<int, int> > v;

int main () {

    cin >> n;
    v.resize (n);
    for ( int i = 0; i < n; i ++ ) cin >> v[i].first >> v[i].second;

    sort (v.begin (), v.end ());

    int st = -2e9, ed = -2e9;
    for ( auto x : v ) {
        if ( ed < x.first ) {
            if ( st != -2e9 ) v.push_back ({ st, ed });
            st = x.first, ed = x.second;
        } else {
            ed = max (ed, x.second);
        }
    }
    if ( st != -2e9 ) v.push_back ({ st, ed });

    v.erase (a.begin (), a.begin () + n);

    cout << v.size () << endl;

    return 0;
}