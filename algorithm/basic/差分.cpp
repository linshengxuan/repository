#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int n, m;
int a[N], cf[N];

void fun () {
    for ( int i = 1; i <= n; i ++ ) cf[i] = a[i] - a[i - 1];
}

void op (int l, int r, int c) {
    cf[l] += c;
    cf[r + 1] -= c;
}

int main () {

    cin >> n >> m;
    for ( int i = 1; i <= n; i ++ ) cin >> a[i];

    fun ();

    int l, r, c;
    while ( m -- ) {
        cin >> l >> r >> c;
        op (l, r, c);
    }

    for ( int i = 1; i <= n; i ++ ) {
        cf[i] += cf[i - 1];
        cout << cf[i] << " ";
    }

    return 0;
}