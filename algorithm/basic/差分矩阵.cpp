#include <iostream>
using namespace std;

const int N = 1e3 + 10;

int n, m, q, x;
int a[N][N], cf[N][N];

void insert (int l1, int r1, int l2, int r2, int c) {
    cf[l1][r1] += c;
    cf[l1][r2 + 1] -= c;
    cf[l2 + 1][r1] -= c;
    cf[l2 + 1][r2 + 1] += c;
}

int main () {

    cin >> n >> m >> q;
    for ( int i = 1; i <= n; i ++ ) {
        for ( int j = 1; j <= m; j ++ ) {
            cin >> x;
            insert (i, j, i, j, x);
        }
    }

    int x1, x2, y1, y2, c;
    while ( q -- ) {
        cin >> x1 >> y1 >> x2 >> y2 >> c;
        insert (x1, y1, x2, y2, c);
    }

    for ( int i = 1; i <= n; i ++ ) {
        for ( int j = 1; j <= m; j ++ ) {
            cf[i][j] += cf[i - 1][j] + cf[i][j - 1] - cf[i - 1][j - 1];
            cout << cf[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}