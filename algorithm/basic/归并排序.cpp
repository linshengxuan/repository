#include <iostream>
using namespace std;
const int N = 1e5 + 10;

int q[N], n;
int a[N];

void merge_sort (int q[], int l, int r) {
    if ( l >= r ) return;
    int mid = l + r >> 1;
    merge_sort (q, l, mid), merge_sort (q, mid + 1, r);
    int i = l, j = mid + 1, k = 0;
    while ( i <= mid && j <= r ) {
        if ( q[i] <= q[j] ) a[k ++] = q[i ++];
        else a[k ++] = q[j ++];
    }
    while ( i <= mid ) a[k ++] = q[i ++];
    while ( j <= r ) a[k ++] = q[j ++];
    for ( int i = 0, j = l; j <= r; i ++, j ++ ) q[j] = a[i];
}

int main () {
    cin >> n;
    for ( int i = 0; i < n; i ++ ) cin >> q[i];
    merge_sort (q, 0, n - 1);
    for ( int i = 0; i < n; i ++ ) cout << q[i] << " ";
    return 0;
}