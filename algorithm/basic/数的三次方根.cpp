#include <iostream>
#include <cmath>
using namespace std;

int main () {

    double x;
    cin >> x;
    double l = -10000, r = 10000;
    while ( r - l > 1e-8 ) {
        double mid = ( l + r ) / 2;
        if ( pow (mid, 3) >= x ) r = mid;
        else l = mid;
    }

    printf ("%.6lf", l);

    return 0;
}