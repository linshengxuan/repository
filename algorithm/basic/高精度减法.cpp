#include <iostream>
#include <vector>
#include <string>
using namespace std;

vector<int> A, B;
string a, b;
int flag;

bool cmp () {
    if ( A.size () != B.size () ) return A.size () > B.size ();

    for ( int i = A.size () - 1; i >= 0; i -- ) {
        if ( A[i] != B[i] ) return A[i] > B[i];
    }
    return true;
}

vector<int> sub (vector<int> &A, vector<int> &B) {
    if ( !cmp () && !flag ) { // 千万要 !flag
        flag = 1;
        return sub (B, A);
    }

    vector <int> c;
    int t = 0;

    for ( int i = 0; i < A.size (); i ++ ) {
        t += A[i];
        if ( i < B.size () ) t -= B[i];
        c.push_back (( t + 10 ) % 10);
        t = t < 0 ? -1 : 0;
    }
    while ( c.back () == 0 && c.size () > 1 ) c.pop_back ();
    return c;
}


int main () {

    cin >> a >> b;
    for ( int i = a.size () - 1; i >= 0; i -- ) A.push_back (a[i] ^ 48);
    for ( int i = b.size () - 1; i >= 0; i -- ) B.push_back (b[i] ^ 48);

    auto c = sub (A, B);
    if ( flag ) cout << "-";
    for ( int i = c.size () - 1; i >= 0; i -- ) cout << c[i];

    return 0;
}