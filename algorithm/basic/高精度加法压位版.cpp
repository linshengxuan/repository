#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

const int base = 1e9;

string a, b;
vector<int> A, B;

vector <int> add (vector<int> &A, vector<int> &B) {

    if ( A.size () < B.size () ) return add (B, A);

    vector <int> c;
    int t = 0;
    
    for ( int i = 0; i < A.size (); i ++ ) {
        t += A[i];
        if ( i < B.size () ) t += B[i];
        c.push_back (t % base);
        t /= base;
    }
    if ( t ) c.push_back (t);
    return c;
}

int main () {

    cin >> a >> b;
    // 一次存九位 九位加法 一定小于 2 * 1e10 没有超过 int 类型
    for ( int i = a.size () - 1, s = 0, j = 0, t = 1; i >= 0; i -- ) {
        s += ( a[i] ^ 48 ) * t;
        j ++, t *= 10;
        if ( j == 9 || i == 0 ) {
            A.push_back (s);
            s = j = 0, t = 1;
        }
    }

    for ( int i = b.size () - 1, s = 0, j = 0, t = 1; i >= 0; i -- ) {
        s += ( b[i] ^ 48 ) * t;
        j ++, t *= 10;
        if ( j == 9 || i == 0 ) {
            B.push_back (s);
            s = j = 0, t = 1;
        }
    }

    vector<int> c = add (A, B);
    cout << c.back ();
    for ( int i = c.size () - 2; i >= 0; i -- ) cout << setw(9) << setfill('0') << c[i];


    return 0;
}