#include <iostream>
#include <string>
#include <vector>
using namespace std;

string a, b;
vector<int> A, B;

vector<int> mul (vector<int> &A, vector<int> &B) {

    vector<int> c(A.size() + B.size());
    
    for ( int i = 0; i < A.size (); i ++ )
        for ( int j = 0; j < B.size (); j ++ )
            c[i + j] += A[i] * B[j];

    for ( int i = 0, t = 0; i < c.size (); i ++ ) {
        t += c[i];
        c[i] = t % 10;
        t /= 10;
    }

    while ( c.size () > 1 && c.back () == 0 ) c.pop_back ();
    return c;

}

int main () {

    cin >> a >> b;
    for ( int i = a.size () - 1; i >= 0; i -- ) A.push_back (a[i] ^ 48);
    for ( int i = b.size () - 1; i >= 0; i -- ) B.push_back (b[i] ^ 48);

    auto c = mul (A, B);
    for ( int i = c.size () - 1; i >= 0; i -- ) cout << c[i];

    return 0;
}