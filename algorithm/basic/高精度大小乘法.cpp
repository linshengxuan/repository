#include <iostream>
#include <vector>
#include <string>
using namespace std;

string a;
int b;
vector<int> A, B;

vector<int> mul (vector<int> &A, int b) {
    vector<int> c;
    int t = 0;
    for ( int i = 0; i < A.size (); i ++ ) {
        t += A[i] * b;
        c.push_back (t % 10);
        t /= 10;
    }
    while ( t ) {
        c.push_back (t % 10);
        t /= 10;
    }
    while ( c.back () == 0 && c.size () > 1 ) c.pop_back ();
    return c;
}

int main () {

    cin >> a >> b;
    for ( int i = a.size () - 1; i >= 0; i -- ) A.push_back (a[i] ^ 48);

    auto c = mul (A, b);
    for ( int i = c.size () - 1; i >= 0 ; i -- ) cout << c[i];

    return 0;
}