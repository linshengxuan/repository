#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int b;
string a;
vector<int> A;

vector<int> div (vector<int> &A, int b) {
    vector<int> c;
    int t = 0;
    for ( int i = 0; i < A.size (); i ++ ) {
        t = t * 10 + A[i];
        c.push_back (t / b);
        t %= b;
    }
    reverse (c.begin (), c.end ());
    while ( c.size () > 1 && c.back () == 0 ) c.pop_back ();
    c.push_back (t);
    return c;
}

int main () {

    cin >> a >> b;
    for ( int i = 0; i < a.size (); i ++ ) A.push_back (a[i] ^ 48);

    auto c = div (A, b);
    for ( int i = c.size () - 2; i >= 0; i -- ) cout << c[i];
    cout << endl << c[c.size () - 1];

    return 0;
}