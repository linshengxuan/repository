#include <iostream>
using namespace std;

const int N = 1e5 + 10, M = 1e6 + 10;

int n, m, ne[N];
char s[M], p[N];

int main () {

    cin >> n >> p + 1 >> m >> s + 1;

    // 求 next 数组
    for ( int i = 2, j = 0; i <= n; i ++ ) {
        while ( j && p[i] != p[j + 1] ) j = ne[j];
        if ( p[i] == p[j + 1] ) j ++ ;
        ne[i] = j;
    }

    // kmp
    for ( int i = 1, j = 0; i <= m; i ++ ) {
        while ( j && s[i] != p[j + 1] ) j = ne[j];
        if ( s[i] == p[j + 1] ) j ++ ;
        if ( j == n ) {
            cout << i - n << " ";
            j = ne[j];
        }
    }


    return 0;
}