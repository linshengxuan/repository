#include <iostream>
using namespace std;

const int N = 3e6 + 10;

int n, cnt[N], son[N][26], idx;
char str[N];

void insert (char s[]) {
    int p = 0;
    for ( int i = 0; s[i]; i ++ ) {
        int u = s[i] - 'a';
        if ( !son[p][u] ) son[p][u] = ++ idx;
        p = son[p][u];
    }
    cnt[p] ++ ;
}

int query (char s[]) {
    int p = 0;
    for ( int i = 0; s[i]; i ++ ) {
        int u = s[i] - 'a';
        if ( !son[p][u] ) return 0;
        else p = son[p][u];
    }
    return cnt[p];
}

int main () {

    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        char op[2];
        cin >> op >> str;
        if ( op[0] == 'I' ) insert (str);
        else cout << query (str) << endl;
    }

    return 0;
}