#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int stk[N], head;

int main () {

    int n, x;
    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        cin >> x;
        while ( head && x <= stk[head] ) head --;
        if ( head ) cout << stk[head] << ' ';
        else cout << -1 << ' ';
        stk[++ head] = x;
    }

    return 0;
}