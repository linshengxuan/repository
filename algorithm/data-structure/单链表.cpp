#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int m;
int e[N], ne[N], idx, head = -1;

// 头部插入
void insert_head (int x) {
    e[idx] = x;
    ne[idx] = head;
    head = idx ++ ;
}

// 第 k 个数后插入
void insert_k (int k, int x) {
    e[idx] = x;
    ne[idx] = ne[k];
    ne[k] = idx ++ ;
}

// 删除第 k 个数后的一个数
void delete_k (int k) {
    ne[k] = ne[ne[k]];
}


int main () {

    cin >> m;
    for ( int i = 0; i < m; i ++ ) {
        char op;
        cin >> op;
        if ( op == 'H' ) {
            int x;
            cin >> x;
            insert_head (x);
        } else if ( op == 'D' ) {
            int k;
            cin >> k;
            if ( !k ) head = ne[head];
            else delete_k (k - 1);
        } else {
            int x, k;
            cin >> k >> x;
            insert_k (k - 1, x);
        }
    }

    for ( int i = head; ~i; i = ne[i] ) {
        cout << e[i] << ' ';
    }

    return 0;
}