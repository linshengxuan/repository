#include <iostream>
using namespace std;

const int N = 1e5 + 10, M = 2 * N;

int n;
int e[N], lt[N], rt[N], idx;

// 初始化
void init () {
    lt[1] = 0;
    rt[0] = 1;
    idx = 2;
}

// 插入
void insert (int k, int x) {
    e[idx] = x;
    rt[idx] = rt[k];
    lt[rt[k]] = idx;
    rt[k] = idx;
    lt[idx] = k;
    idx ++ ;
}

// 删除
void del (int k) {
    rt[lt[k]] = rt[k];
    lt[rt[k]] = lt[k];
}

int main () {

    init () ;

    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        int k, x;
        string op;
        cin >> op;
        if ( op == "L" ) {
            cin >> x;
            insert (0, x);
        } else if ( op == "R" ) {
            cin >> x;
            insert (lt[1], x);
        } else if ( op == "D" ) {
            cin >> k;
            del (k + 1);
        } else if ( op == "IL" ) {
            cin >> k >> x;
            insert (lt[k + 1], x);
        } else {
            cin >> k >> x;
            insert (k + 1, x);
        }
    }

    for ( int i = rt[0]; i != 1; i = rt[i] ) {
        cout << e[i] << ' ';
    }

    return 0;
}