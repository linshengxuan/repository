#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int h[N];

void init () {
    for ( int i = 0; i < N; i ++ ) h[i] = i;
}

int find (int x) {
    if ( h[x] != x ) h[x] = find (h[x]);
    return h[x];
}

int main () {

    init ();

    int n, m;
    cin >> n >> m;
    for ( int i = 0; i < m; i ++ ) {
        char op;
        int a, b;
        cin >> op >> a >> b;
        if ( op == 'M' ) {
            if ( find (a) == find (b) ) continue;
            else h[find (a)] = find (b);
        } else {
            if ( find (a) == find (b) ) cout << "Yes" << endl;
            else cout << "No" << endl;
        }
    }

    return 0;
}