#include <iostream>
using namespace std;
const int N = 1e5 + 10;

int n, m, heap[N], idx;

void down (int u) {
    int t = u;
    if ( u << 1 <= idx && heap[u << 1] < heap[t] ) t = u << 1;
    if ( ( u << 1 ) + 1 <= idx && heap[( u << 1 ) + 1] < heap[t] ) t = ( u << 1 ) + 1;
    if ( t != u ) {
        swap (heap[u], heap[t]);
        down (t);
    }
}

int main () {

    cin >> n >> m;
    for ( int i = 0; i < n; i ++ ) cin >> heap[++ idx];
    for ( int i = n >> 1; i; i -- ) down (i);
    for ( int i = 0; i < m; i ++ ) {
        cout << heap[1] << ' ';
        heap[1] = heap[idx --];
        down (1);
    }

    return 0;
}