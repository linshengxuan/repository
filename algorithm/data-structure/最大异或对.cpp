#include <iostream>
using namespace std;

const int N = 6e6 + 10;

int n, res;
int trie[N][2], idx;

void insert (int x) {
    int p = 0;
    for ( int i = 30; ~i; i -- ) {
        int u = x >> i & 1;
        if ( !trie[p][u] ) trie[p][u] = ++ idx;
        p = trie[p][u];
    }
}

int query (int x) {
    int ans = 0, p = 0;
    for ( int i = 30; ~i; i -- ) {
        int u = x >> i & 1;
        if ( trie[p][u ^ 1] ) {
            p = trie[p][u ^ 1];
            ans = ans << 1 | ( u ^ 1 );
        } else {
            p = trie[p][u];
            ans = ans << 1 | u;
        }
    }
    return ans;
}

int main () {

    cin >> n;

    for ( int i = 0, x = 0; i < n; i ++ ) {
        cin >> x;
        res = max (res, x ^ query (x));
        insert (x);
    }
    cout << res << endl;

    return 0;
}