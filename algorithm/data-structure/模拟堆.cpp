#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int n, k, x, heap[N];
int idx, indx[N], ct, kth[N];
string op;

void sswap (int idx1, int idx2) {
    swap (indx[kth[idx1]], indx[kth[idx2]]);
    swap (kth[idx1], kth[idx2]);
    swap (heap[idx1], heap[idx2]);
}

void up (int u) {
    while ( u >> 1 && heap[u >> 1] > heap[u] ) {
        sswap (u >> 1, u);
        u >>= 1;
    }
}

void down (int u) {
    int t = u;
    if ( u << 1 <= idx && heap[u << 1] < heap[t] ) t = u << 1;
    if ( u << 1 ^ 1 <= idx && heap[u << 1 ^ 1] < heap[t] ) t = u << 1 ^ 1;
    if ( u != t ) {
        sswap (u, t);
        down (t);
    }
}

int main () {

    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        cin >> op;
        if ( op == "I" ) {
            cin >> x;
            ++ ct, ++ idx;
            indx[ct] = idx, kth[idx] = ct, heap[idx] = x;
            up (idx);
        } else if ( op == "PM" ) {
            cout << heap[1] << endl;
        } else if ( op == "DM" ) {
            sswap (1, idx --);
            down (1);
        } else if ( op == "D" ) {
            cin >> k;
            k = indx[k];
            sswap (k, idx --);
            down (k), up (k);
        } else {
            cin >> k >> x;
            k = indx[k];
            heap[k] = x;
            down (k), up (k);
        }
    }


    return 0;
}