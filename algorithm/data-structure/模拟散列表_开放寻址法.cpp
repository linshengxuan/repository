#include <iostream>
#include <cstring>
using namespace std;

const int N = 2e5 + 3, null = 0x3f3f3f3f;

int h[N], n, x;
char op;

int find (int x) {
    int mod = ( x % N + N ) % N;
    while ( h[mod] != null && h[mod] != x ) {
        mod ++;
        if ( mod == N ) mod = 0;
    }
    return mod;
}

int main () {

    memset (h, 0x3f, sizeof h);
    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        cin >> op >> x;
        if ( op == 'I' ) h[find (x)] = x;
        else
            if ( h[find (x)] != null ) cout << "Yes" << endl;
            else cout << "No" << endl;
    }

    return 0;
}