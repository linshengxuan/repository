#include <iostream>
#include <cstring>
using namespace std;

// 质数
const int N = 1e5 + 3;

int n, x;
char op;
int h[N], e[N], ne[N], idx;

void insert (int x) {
    int mod = ( x % N + N ) % N;
    e[++ idx] = x, ne[idx] = h[mod], h[mod] = idx;
}

bool find (int x) {
    int mod = ( x % N + N ) % N;
    for ( int i = h[mod]; ~i; i = ne[i] )
        if ( e[i] == x ) return true;
    return false;
}

int main () {

    memset (h, -1, sizeof h);

    cin >> n;
    for ( int i = 0; i < n; i ++ ) {
        cin >> op >> x;
        if ( op == "I" ) insert (x);
        else
            if ( find (x) ) cout << "Yes" << endl;
            else cout << "No" << endl;
    }

    return 0;
}