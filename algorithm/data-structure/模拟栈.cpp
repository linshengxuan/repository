#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int stk[N], head;
string ss[] = {
    "push", "pop", "empty", "query"
};

int main () {

    int n, x;
    cin >> n;
    while ( n -- ) {
        string s;
        cin >> s;
        if ( s == ss[0] ) {
            cin >> x;
            stk[++ head] = x;
        } else if ( s == ss[1] ) {
            head --;
        } else if ( s == ss[2] ) {
            if ( !head ) cout << "YES" << endl;
            else cout << "NO" << endl;
        } else {
            cout << stk[head] << endl;
        }
    }

    return 0;
}