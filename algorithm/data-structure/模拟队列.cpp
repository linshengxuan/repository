#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int q[N], head, ed;

string ss[] = {
    "push", "pop", "empty", "query"
};

int main () {

    int n, x;
    cin >> n;
    while ( n -- ) {
        string s;
        cin >> s;
        if ( s == ss[0] ) {
            cin >> x;
            q[ed ++] = x;
        } else if ( s == ss[1] ) {
            ++ head;
        } else if ( s == ss[2] ) {
            if ( ed <= head ) cout << "YES" << endl;
            else cout << "NO" << endl;
        } else {
            cout << q[head] << endl;
        }
    }

    return 0;
}