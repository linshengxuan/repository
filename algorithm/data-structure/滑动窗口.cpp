#include <iostream>
using namespace std;

const int N = 1e6 + 10;

int q[N], idx[N], n, k, ed, st;

int main () {

    cin >> n >> k;
    for ( int i = 0; i < n; i ++ ) cin >> q[i];

    // min
    st = ed = 0;
    for ( int i = 0; i < n; i ++ ) {
        if ( idx[st] < i - k + 1 ) st ++ ;
        while ( st <= ed && q[idx[ed]] >= q[i] ) ed --;
        idx[++ ed] = i;
        if ( i >= k - 1 ) cout << q[idx[st]] << ' ';
    }

    cout << endl;

    // max
    st = ed = 0;
    for ( int i = 0; i < n; i ++ ) {
        if ( idx[st] < i - k + 1 ) st ++ ;
        while ( st <= ed && q[idx[ed]] <= q[i] ) ed --;
        idx[++ ed] = i;
        if ( i >= k - 1 ) cout << q[idx[st]] << ' ';
    }

    return 0;
}