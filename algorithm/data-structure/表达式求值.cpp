#include <iostream>
#include <cstring>
#include <stack>
#include <unordered_map>
using namespace std;

stack <int> num;
stack <char> op;
unordered_map <char, int> pr { { '+', 1 }, { '-', 1 }, { '*', 2 }, { '/', 2 } };

void eval () {

    int res = 0;

    int b = num.top (); num.pop ();
    int a = num.top (); num.pop ();
    char x = op.top (); op.pop ();

    switch ( x ) {
        case '+': res = a + b; break;
        case '-': res = a - b; break;
        case '*': res = a * b; break;
        case '/': res = a / b; break;
    }

    num.push (res);

}

int main () {

    string s;
    cin >> s;

    for ( int i = 0; i < s.size (); i ++ ) {
        char c = s[i];
        if ( isdigit (c) ) {
            int x = 0, j = i;
            while ( j < s.size () && isdigit (s[j]) ) x = ( x << 1 ) + ( x << 3 ) + ( s[j ++] ^ 48 );
            i = j - 1;
            num.push (x);
        } else {
            if ( c == '(' ) op.push (c);
            else if ( c == ')' ) {
                while ( op.top () != '(' ) eval ();
                op.pop ();
            } else {
                while ( op.size () && pr[op.top ()] >= pr[c] ) eval ();
                op.push (c);
            }
        }
    }

    while ( op.size () ) eval ();
    cout << num.top () << endl;

    return 0;
}