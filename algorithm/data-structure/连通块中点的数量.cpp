#include <iostream>
using namespace std;

const int N = 1e5 + 10;

int n, m;
int h[N], cnt[N];

void init () {
    for ( int i = 0; i < N; i ++ ) h[i] = i, cnt[i] = 1;
}

int find (int x) {
    if ( h[x] != x ) h[x] = find (h[x]);
    return h[x];
}

int main () {

    init ();

    cin >> n >> m;
    for ( int i = 0; i < m; i ++ ) {
        string op;
        int a, b;
        cin >> op >> a;
        if ( op == "C" ) {
            // 连接 a b
            cin >> b;
            if ( find (a) == find (b) ) continue;
            cnt[find (b)] += cnt[find (a)];
            h[find (a)] = find (b);
        } else if ( op == "Q1" ) {
            // 判断是否在连通块
            cin >> b;
            if ( find (a) == find (b) ) cout << "Yes" << endl;
            else cout << "No" << endl;
        } else {
            // 连通块点的数量
            cout << cnt[find (a)] << endl;
        }
    }

    return 0;
}