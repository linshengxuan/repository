#include <bits/stdc++.h>
using namespace std;

const int N = 5e4 + 10;

int n, k;
int f[N], d[N];

void init () {
    for ( int i = 0; i < N; i ++ ) f[i] = i;
}

int find (int u) {
    if ( f[u] != u ) {
        int t = find (f[u]);
        d[u] += d[f[u]];
        f[u] = t;
    }
    return f[u];
}

int main () {
    init ();
    cin >> n >> k;
    int res = 0;
    while ( k -- ) {
        int t, x, y;
        cin >> t >> x >> y;
        if ( x > n || y > n ) res ++ ;
        else {
            int q = find (x), w = find (y);
            if ( t == 1 ) {
                if ( q == w && ( d[y] - d[x] ) % 3 ) res ++ ;
                else if ( q != w ) {
                    f[q] = w;
                    d[q] = d[y] - d[x];
                }
            } else {
                if ( q == w && ( d[y] - d[x] - 1 ) % 3 ) res ++ ;
                else if ( q != w ) {
                    f[q] = w;
                    d[q] = d[y] - 1 - d[x];
                }
            }
        }
    }
    cout << res;
    return 0;
}